
#include <string>
#include <iostream>
#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <msclr\marshal_cppstd.h>
#include <nlohmann/json.hpp>

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Data::SqlClient;
using namespace System::Runtime::InteropServices;
ref class DB {
private:
    SqlConnection^ conn;
    SqlConnectionStringBuilder^ connStringBuilder;
public:
    DB();
    int Select(std::string login, std::string password, int *type);
    void ConnectToDB();
    std::string SendNumInQueue1(SOCKET con, int id);
    std::string SendNumInQueue2(SOCKET con, int id);
    std::string SendNumInQueue3(SOCKET con, int id);
    void SendQueue(SOCKET con/*, int queue*/);
    void ChangeQ1(SOCKET con);
    void ChangeQ2(SOCKET con);
    void ChangeQ3(SOCKET con);
    bool ExistOfLogin(std::string login);
    void Insert(std::string name, std::string lastName, std::string login, std::string password);
};